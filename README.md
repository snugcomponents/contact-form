# Quickstart
This extension add contact form to your application.

## Instalation
#### Download
The best way to install `snugcomponents/contact-form` is using Composer:
```sh
$ composer require snugcomponents/contact-form
```
#### Registering
You can enable the extension using your neon config:
```sh
extensions:
	SnugcomponentsContactForm: Snugcomponents\Contact\ContactExtension
```
#### Injecting
You can simply inject factory in Your Presenters/Services:
```php
public function __construct(\Snugcomponents\Contact\Contact\IContactFormFactory $contactFormFactory)
{
    parent::__construct();
    ...
}
```
or You can use trait in presenters:
```php
abstract class BasePresenter extends Nette\Application\UI\Presenter
{
    use \Snugcomponents\Contact\TContactForm;
}
```
and then in .latte:
```php
{control contractForm}
```
## Customization
Component is fully customizable. 
You can set options in config file like this:
```php
SnugcomponentsContactForm:
	template: 'path/to/your/form/template.latte'                        # IMPORTANT template must contain snippet with name 'contactForm'
	title: 'your title'                                                 # title of formular
	sendButtonText: 'your send button text'                             # text which will be shown in send button
	emailReceiver: 'your email receiver'                                # can be more emails separated by ','
	emailSubject: 'your email subject'
	emailSender: 'your email sender'
	emailMessage: 'your email message'                                  # content of email. Must contain tags: $NAME$, $PHONE$, $EMAIL$, $TEXT$
	ReCaptcha:
		siteKey: 'your site key for ReCaptcha'
		secretKey: 'your secret key for ReCaptcha'
		errors:                                                     # In this section are ERROR messages so client does not see them so you don't need to modify them
			missingSiteKey: 'Please insert SiteKey for ReCaptcha'
			missingSecretKey: 'Please insert SecretKey for ReCaptcha'
			missingReCaptchaPOSTdata: 'your recaptcha error if someone deletes recaptcha item from form'
	flashMessage:
		success: 'Message which show after sending email'
		error: 'Message which shows after failure sending email'
	labels:
		name: 'Name'
		mobile: 'Mobile number'
		email: 'E-mail'
		message: 'Message'
		personal_data: 'Accept condition'
	placeholders:
		name: 'Name'
		mobile: 'Mobile number'
		email: 'E-mail'
		message: 'Message'
	errors:
		name: 'required'                                            # set to false if you want to have this input unrequired or not shown. Otherwise set the error message if not filled
		mobile: 'required'                                          # set to false if you want to have this input unrequired or not shown. Otherwise set the error message if not filled
		email: 'required'                                           # set to false if you want to have this input unrequired or not shown. Otherwise set the error message if not filled
		message: 'required'                                         # set to false if you want to have this input unrequired or not shown. Otherwise set the error message if not filled
		personal_data: 'You must accept this'                       # set to false if you want to have this input unrequired or not shown. Otherwise set the error message if not filled
		recaptcha: 'You are robot'
		form:                                                       # In this section are ERROR messages so client does not see them so you don't need to modify them
			missingSender: 'You must fill the E-mail sender'
			missingReceiver: 'You must fill the E-mail receiver'
			missingSubject: 'You must fill the E-mail subject'
			missingMessage: 'You must fill the E-mail message'
			wrongSender: 'E-mail sender must be an email address'
			wrongReceiver: 'E-mail receiver must be an email address'
			message:
				missingName: 'In your E-mail message is missing name tag. Please insert at least one $NAME$ tag in message string.'
				missingPhone: 'In your E-mail message is missing phone tag. Please insert at least one $PHONE$ tag in message string.'
				missingEmail: 'In your E-mail message is missing email tag. Please insert at least one $EMAIL$ tag in message string.'
				missingText: 'In your E-mail message is missing text tag. Please insert at least one $TEXT$ tag in message string.'
```
## ReCaptcha
This extension uses ReCaptcha v2. If you want to use ReCaptcha you must set the ReCaptcha widget into your template. For more information see https://developers.google.com/recaptcha/docs/display
ReCaptcha will not be used if you do not set ReCaptcha secretKey and siteKey in your config file.
## Conclusion
This extension requires Nette2.4 and it is property of SnugDesign � 2020
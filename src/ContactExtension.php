<?php
namespace Snugcomponents\Contact;

use Nette\DI\CompilerExtension;
use Snugcomponents\Contact\Contact\ContactFormFactory;

class ContactExtension extends CompilerExtension
{
    private $defaults = [
        'template' => (__DIR__ . '/samples/simpleTemplate.latte'),
        'title' => 'Contact form',
        'sendButtonText' => 'Send',
        'emailReceiver' => '',
        'emailSubject' => '',
        'emailSender' => '',
        'emailMessage' => '',
        'ReCaptcha' => [
            'siteKey' => '',
            'secretKey' => '',
            'errors' => [
                'missingSiteKey' => 'Please insert SiteKey for ReCaptcha',
                'missingSecretKey' => 'Please insert SecretKey for ReCaptcha',
                'missingReCaptchaPOSTdata' => 'In your POST request is not ReCaptcha part. Please insert in your mannualy rendered form tag \'<div class="g-recaptcha" id="g-recaptcha" data-sitekey="%YOUR_SITE_KEY%"></div>\' and include ReCaptcha libraries. See: https://developers.google.com/recaptcha/docs/display'
            ]
        ],
        'flashMessage' => [
            'success' => 'Your message has been succesfully send.',
            'error' => 'We have encountered an error. Please try it again later.',
        ],
        'labels' => [
            ContactFormFactory::FORM_INPUT_NAME => 'Name',
            ContactFormFactory::FORM_INPUT_MOBILE => 'Mobile number',
            ContactFormFactory::FORM_INPUT_EMAIL => 'E-mail',
            ContactFormFactory::FORM_INPUT_MESSAGE => 'Message',
            ContactFormFactory::FORM_INPUT_PERSONAL_DATA => 'Accept condition',
        ],
        'placeholders' => [
            ContactFormFactory::FORM_INPUT_NAME => 'Name',
            ContactFormFactory::FORM_INPUT_MOBILE => 'Mobile number',
            ContactFormFactory::FORM_INPUT_EMAIL => 'E-mail',
            ContactFormFactory::FORM_INPUT_MESSAGE => 'Message',
        ],
        'errors' => [
            ContactFormFactory::FORM_INPUT_NAME => 'required',
            ContactFormFactory::FORM_INPUT_MOBILE => 'required',
            ContactFormFactory::FORM_INPUT_EMAIL => 'required',
            ContactFormFactory::FORM_INPUT_MESSAGE => 'required',
            ContactFormFactory::FORM_INPUT_PERSONAL_DATA => 'You must accept this',
            ContactFormFactory::FORM_INPUT_RECAPTCHA => 'You are robot',
            'form' => [
                'missingSender' => 'You must fill the E-mail sender',
                'missingReceiver' => 'You must fill the E-mail receiver',
                'missingSubject' => 'You must fill the E-mail subject',
                'missingMessage' => 'You must fill the E-mail message',
                'wrongSender' => 'E-mail sender must be an email address',
                'wrongReceiver' => 'E-mail receiver must be an email address',
                'message' => [
                    'missingName' => 'In your E-mail message is missing name tag. Please insert at least one $NAME$ tag in message string.',
                    'missingPhone' => 'In your E-mail message is missing phone tag. Please insert at least one $PHONE$ tag in message string.',
                    'missingEmail' => 'In your E-mail message is missing email tag. Please insert at least one $EMAIL$ tag in message string.',
                    'missingText' => 'In your E-mail message is missing text tag. Please insert at least one $TEXT$ tag in message string.',
                ]
            ]
        ]
    ];
    
    public function loadConfiguration()
    {
        $defaults = $this->validateConfig($this->defaults);
        $builder = $this->getContainerBuilder();
        
        $this->compiler->loadDefinitions(
            $builder,
            $this->loadFromFile(__DIR__ . '/config/common.neon')['services'],
            $this->name
        );
        
        $builder->getDefinition('SnugcomponentsContactForm.SnugcomponentsContactFormFactory')
                ->addSetup('setSendButtonText', [$defaults['sendButtonText']])
                ->addSetup('setLabels', [$defaults['labels']])
                ->addSetup('setErrors', [$defaults['errors']])
                ->addSetup('setPlaceholders', [$defaults['placeholders']])
                ->addSetup('setEmailSender', [$defaults['emailSender']])
                ->addSetup('setEmailReceiver', [$defaults['emailReceiver']])
                ->addSetup('setEmailSubject', [$defaults['emailSubject']])
                ->addSetup('setEmailMessage', [$defaults['emailMessage']]);
        
        $builder->getDefinition('SnugcomponentsContactForm.SnugcomponentsContactForm')
                ->addSetup('setTitle', [$defaults['title']])
                ->addSetup('setTemplatePath', [$defaults['template']])
                ->addSetup('setFlashMessage', [$defaults['flashMessage']])
                ->addSetup('setReCaptchaErrors', [$defaults['ReCaptcha']['errors']])
                ->addSetup('setReCaptchaSiteKey', [$defaults['ReCaptcha']['siteKey']])
                ->addSetup('setReCaptchaSecretKey', [$defaults['ReCaptcha']['secretKey']]);
    }
}

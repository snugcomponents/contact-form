<?php

namespace Snugcomponents\Contact;

use Snugcomponents\Contact\Contact\IContactFormFactory;

trait TContactForm
{
    /**
     * @var IContactFormFactory
     */
    private $contactFormFactory;
    
    public function injectIContactFormFactory(IContactFormFactory $contactFormFactory)
    {
        $this->contactFormFactory = $contactFormFactory;
    }
    
    public function createComponentContactForm()
    {
        return $this->contactFormFactory->create();
    }
}

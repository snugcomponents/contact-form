<?php

namespace Snugcomponents\Contact\Contact;

use Nette\Application\UI\Form;
use Nette\Localization\ITranslator;
use Nette\Utils\ArrayHash;
use Nette\Utils\Validators;
use Nette\Utils\Strings;
use Nette\Mail\Message;
use Nette\Mail\SendmailMailer;

class ContactFormFactory
{

    use \Nette\SmartObject;
    
    const 
            FORM_INPUT_NAME = 'name',
            FORM_INPUT_MOBILE = 'mobile',
            FORM_INPUT_EMAIL = 'email',
            FORM_INPUT_MESSAGE = 'message',
            FORM_INPUT_PERSONAL_DATA = 'personal_data',
            FORM_INPUT_RECAPTCHA = 'recaptcha';
    
    /**
     * @var ITranslator
     */
    private $translator;

    /**
     * @var array
     */
    private $labels;

    /**
     * @var array
     */
    private $placeholders;
    
    /**
     * @var array
     */
    private $errors;

    /**
     * @var string
     */
    private $sendButtonText;

    /**
     * @var string
     */
    private $sender;

    /**
     * @var string
     */
    private $receiver;

    /**
     * @var string
     */
    private $subject;

    /**
     * @var string
     */
    private $message;
    
    public function __construct(
            ITranslator $translator = null)
    {
        $this->translator = $translator;
        $this->message = '';
    }
    
    public function create()
    {
        $this->checkRequirements();
        
        $form = new Form();
        $form->setTranslator($this->translator);
        $formPrototype = $form->getElementPrototype();
        $formPrototype->setAttribute('novalidate', 'novalidate');
        
        $form->addText(self::FORM_INPUT_NAME, $this->labels[self::FORM_INPUT_NAME])
                ->setRequired($this->errors[self::FORM_INPUT_NAME])
                ->setAttribute('placeholder', $this->placeholders[self::FORM_INPUT_NAME]);
        
        $form->addText(self::FORM_INPUT_MOBILE, $this->labels[self::FORM_INPUT_MOBILE])
                ->setRequired($this->errors[self::FORM_INPUT_MOBILE])
                ->setAttribute('placeholder', $this->placeholders[self::FORM_INPUT_MOBILE]);
        
        $form->addEmail(self::FORM_INPUT_EMAIL, $this->labels[self::FORM_INPUT_EMAIL])
                ->setRequired($this->errors[self::FORM_INPUT_EMAIL])
                ->setAttribute('placeholder', $this->placeholders[self::FORM_INPUT_EMAIL]);
        
        $form->addTextArea(self::FORM_INPUT_MESSAGE, $this->labels[self::FORM_INPUT_MESSAGE], 10, 10)
                ->setRequired($this->errors[self::FORM_INPUT_MESSAGE])
                ->setAttribute('placeholder', $this->placeholders[self::FORM_INPUT_MESSAGE]);
        
        $form->addCheckbox(self::FORM_INPUT_PERSONAL_DATA, $this->labels[self::FORM_INPUT_PERSONAL_DATA])
                ->setRequired($this->errors[self::FORM_INPUT_PERSONAL_DATA]);
        
        $form->onSuccess[] = [$this, 'onSuccess'];
        
        $form->addSubmit('submit', $this->sendButtonText);
        
        return $form;
    }
    
    public function checkRequirements()
    {
        if (!Strings::contains($this->message, '$NAME$')) {
            $this->throwException($this->errors['form']['message']['missingName'], 1);
        }
        if (!Strings::contains($this->message, '$PHONE$')) {
            $this->throwException($this->errors['form']['message']['missingPhone'], 2);
        }
        if (!Strings::contains($this->message, '$EMAIL$')) {
            $this->throwException($this->errors['form']['message']['missingEmail'], 3);
        }
        if (!Strings::contains($this->message, '$TEXT$')) {
            $this->throwException($this->errors['form']['message']['missingText'], 4);
        }
        if (!$this->sender) {
            $this->throwException($this->errors['form']['missingSender'], 5);
        }
        if (!$this->receiver) {
            $this->throwException($this->errors['form']['missingReceiver'], 6);
        }
        if (!$this->subject) {
            $this->throwException($this->errors['form']['missingSubject'], 7);
        }
        if (!$this->message) {
            $this->throwException($this->errors['form']['missingMessage'], 8);
        }
        if (!Validators::isEmail($this->sender)) {
            $this->throwException($this->errors['form']['wrongSender'], 9);
        }
        foreach ($this->receiver as $receiver) {
            if (!Validators::isEmail($receiver)) {
                $this->throwException($this->errors['form']['wrongReceiver'], 10);
            }
        }
    }
    
    public function onSuccess(Form $form, ArrayHash $values)
    {
        $this->message = str_replace('$NAME$', '%1$s', $this->message);
        $this->message = str_replace('$PHONE$', '%2$s', $this->message);
        $this->message = str_replace('$EMAIL$', '%3$s', $this->message);
        $this->message = str_replace('$TEXT$', '%4$s', $this->message);
        
        $message = new Message();
        $message->setFrom($this->sender);
        foreach ($this->receiver as $receiver) {
            $message->addTo($receiver);
        }
        $message->setSubject($this->subject);
        $message->setHtmlBody(sprintf(  $this->message, 
                                        $values->{self::FORM_INPUT_NAME}, 
                                        $values->{self::FORM_INPUT_MOBILE}, 
                                        $values->{self::FORM_INPUT_EMAIL}, 
                                        $values->{self::FORM_INPUT_MESSAGE}));
                                        
        $sender = new SendmailMailer();
        $sender->send($message);
    }
    
    public function setLabels(array $labels)
    {
        $this->labels = $labels;
    }
    
    public function setPlaceholders(array $placeholders)
    {
        $this->placeholders = $placeholders;
    }
    
    public function setErrors(array $errors)
    {
        $this->errors = $errors;
    }
    
    public function setSendButtonText($text)
    {
        $this->sendButtonText = $text;
    }
    
    public function setEmailSender($sender)
    {
        $this->sender = $sender;
    }
    
    public function setEmailReceiver($receiver)
    {
        $this->receiver = explode(',', str_replace(' ', '', $receiver));
    }
    
    public function setEmailSubject($subject)
    {
        $this->subject = $this->translate($subject);
    }
    
    public function setEmailMessage($message)
    {
        $this->message = $this->translate($message);
    }
    
    public function translate($text)
    {
        if ($this->translator) {
            return $this->translator->translate($text);
        } else {
            return $text;
        }
    }
    
    public function throwException($text, $code)
    {
        throw new ContactFormException($this->translate($text), $code);
    }
    
}
<?php

namespace Snugcomponents\Contact\Contact;

use Nette\Application\UI\Control;
use Nette\Localization\ITranslator;
use Nette\Http\Request;
use Nette\Application\UI\Form;
use Nette\Utils\Json;
use Tracy\Debugger;

use Snugcomponents\Contact\Contact\ContactFormFactory;

class ContactFormControl extends Control
{

    /**
     * @var Request
     */
    private $httpRequest;

    /**
     * @var ContactFormFactory
     */
    private $contactFormFactory;

    /**
     * @var ITranslator
     */
    private $translator;

    /**
     * @var string
     */
    private $templatePath;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string[]
     */
    private $messages;

    /**
     * @var string
     */
    private $sender;

    /**
     * @var string
     */
    private $receiver;

    /**
     * @var string
     */
    private $subject;

    /**
     * @var string
     */
    private $message;

    /**
     * @var string
     */
    private $secretKey;

    /**
     * @var string
     */
    private $siteKey;

    /**
     * @var string[]
     */
    private $reCaptchaErrors;

    /**
     * @var bool
     */
    private $hasReCaptcha;

    public function __construct(
            ContactFormFactory $contactFormFactory,
            Request $request,
            ITranslator $translator = null)
    {
        $this->translator = $translator;
        $this->contactFormFactory = $contactFormFactory;
        $this->httpRequest = $request;
    }
    
    public function render()
    {
        $this->verifyReCaptcha();
        
        $this->template->siteKey = $this->siteKey;
        $this->template->hasReCaptcha = $this->hasReCaptcha;
        $this->template->title = $this->title;
        
        $this->template->setTranslator($this->translator);
        $this->template->render($this->templatePath);
    }
    
    public function createComponentContactForm()
    {
        $this->verifyReCaptcha();
        if ($this->sender) {
            $this->contactFormFactory->setEmailSender($this->sender);
        }
        if ($this->receiver) {
            $this->contactFormFactory->setEmailReceiver($this->receiver);
        }
        if ($this->subject) {
            $this->contactFormFactory->setEmailSubject($this->subject);
        }
        if ($this->message) {
            $this->contactFormFactory->setEmailMessage($this->message);
        }
        $form = $this->contactFormFactory->create();
        $form->onValidate[] = [$this, 'onValidate'];
        $form->onSuccess[] = [$this, 'onSuccess'];
        return $form;
    }
    
    public function onValidate(Form $form)
    {
        if ($this->hasReCaptcha) {
            if (empty($this->httpRequest->getPost('g-recaptcha-response'))) {
                //$this->contactFormFactory->throwException($this->reCaptchaErrors['missingReCaptchaPOSTdata'], 13);
                $form->addError('Recaptcha');
            }
            if (!$this->checkReCaptcha($this->httpRequest->getPost('g-recaptcha-response'))) {
                $form->addError('Recaptcha');
            }
        }
        
        if ($this->httpRequest->isAjax()) {
            $this->redrawControl('contactForm');
        }
    }
    
    private function checkReCaptcha($response)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"https://www.google.com/recaptcha/api/siteverify");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query([
            'secret' => $this->secretKey,
            'response' => $response
        ]));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = Json::decode(curl_exec($ch));
        curl_close($ch);

        if (!$server_output->success) {
            Debugger::log($server_output);
        }
        
        return $server_output->success;
    }
    
    public function onSuccess()
    {
        $this->flashMessage($this->contactFormFactory->translate($this->messages['success']), 'success');
    }
    
    public function setTitle($title)
    {
        $this->title = $title;
    }
    
    public function setTemplatePath($path)
    {
        $this->templatePath = $path;
    }
    
    public function setFlashMessage(array $messages)
    {
        $this->messages = $messages;
    }
    
    public function flashMessage($message, $type = 'info')
    {
        if ($this->translator) {
            $message = $this->translator->translate($message);
        }
        return parent::flashMessage($message, $type);
    }
    
    public function setEmailSender($sender)
    {
        $this->sender = $sender;
    }
    
    public function setEmailReceiver($receiver)
    {
        $this->receiver = $receiver;
    }
    
    public function setEmailSubject($subject)
    {
        $this->subject = $subject;
    }
    
    public function setEmailMessage($message)
    {
        $this->message = $message;
    }
    
    public function setReCaptchaSiteKey($siteKey)
    {
        $this->siteKey = $siteKey;
    }
    
    public function setReCaptchaSecretKey($secretKey)
    {
        $this->secretKey = $secretKey;
    }
    
    public function setReCaptchaErrors($errors)
    {
        $this->reCaptchaErrors = $errors;
    }
    
    private function verifyReCaptcha()
    {
        if (isset($this->hasReCaptcha)) {
            return $this->hasReCaptcha;
        }
        
        $this->hasReCaptcha = !(empty($this->siteKey) && empty($this->secretKey));
        if (!$this->hasReCaptcha) {
            return;
        }
        
        if (empty($this->siteKey)) {
            $this->contactFormFactory->throwException($this->reCaptchaErrors['missingSiteKey'], 11);
        }
        
        if (empty($this->secretKey)) {
            $this->contactFormFactory->throwException($this->reCaptchaErrors['missingSecretKey'], 12);
        }
    }
}
<?php

namespace Snugcomponents\Contact\Contact;

interface IContactFormFactory
{
    /**
     * @return ContactFormControl
     */
    public function create();
}
